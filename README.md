# README #

Questo progetto è il risultato di una hackathon online della durata di quattro giorni.
Rappresenta il tentativo di creare un motore automatico di certificazione di aree di città in termini di qualità della vita, utilizzando le ultimissime tecnologie in Cloud.

### How do I get set up? ###

È previsto il running solo in dev mode attualmente. 
Dopo aver scarito il repo lanciare 'npm install' nella cartella di progetto.
L' entry point del progetto è 'index.js'.

### AUTHORS
Alessio Proietti <alessio.proietti@protonmail.com>