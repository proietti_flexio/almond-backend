//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
var rest = require('./rest.js');
var api = require('./api.json')

function harvest() {
        return rest.callToServiceAt(api.airquality.latest, {
            latitude: 37.527707,  
            longitude: 15.075302 
        })
}

module.exports = {
    harvest: harvest
}
