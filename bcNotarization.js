//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
var rest = require('./rest.js');
var api = require('./api.json')

//raccolta dati da entity blockchain
function harvest(id) {
        return rest.callToServiceAt(api.bcnotarization.data, {
            idEntity: id || process.env.ENTITY_ID, 
        })
}

//notarizzazione su blockchain
function notarize(value) {
    return rest.callToServiceAt(api.bcnotarization.data, undefined, "post",
        JSON.stringify({
            idEntity: process.env.ENTITY_ID, 
            value: new Buffer.from(JSON.stringify(value)).toString('base64')
        }), "application/json", true)
}

module.exports = {
    harvest: harvest,
    notarize: notarize
}