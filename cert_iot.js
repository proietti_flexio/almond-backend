//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
const bcNotarization = require('./bcNotarization.js');

function harvest() {
    return bcNotarization.harvest("team03_iotentity")
}

module.exports = {
    harvest: harvest
}