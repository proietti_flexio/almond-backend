//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
const fs = require('fs');
var rest = require('./rest.js');
var api = require('./api.json')

function harvest() {
    const contents = fs.readFileSync('./asset/mascherina.pdf', { encoding: 'base64' });
    return rest.callToServiceAt(api.gcloudvision, undefined, "post",
        JSON.stringify({
            requests: [
                {
                    inputConfig: {
                        content: contents,
                        mimeType: "application/pdf"
                    },
                    features: [
                        {
                            type: "OBJECT_LOCALIZATION"
                        }
                    ]
                }
            ]
        }), "application/json")
}

module.exports = {
    harvest: harvest
}