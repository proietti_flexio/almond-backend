//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
require('dotenv').config();

var airQuality = require('./airQuality.js');
var social = require('./social.js');
var cloudVision = require('./cloudVision.js');
var certIoT = require('./cert_iot.js');
var report = require('./report.js');
var pubSub = require('./pubSub.js');
var bcNotarization = require('./bcNotarization.js');

async function main() {

    //Fase 1: i dati vengono raccolti
    var air = await airQuality.harvest();
    var soc = await social.harvest();
    var vision = await cloudVision.harvest();
    var iot = await certIoT.harvest();

    //Fase 2: viene creato un report e vengono assegnati dei voti per ogni ambito 
    //e un voto pesato generale 
    var digest = report.create(air, soc, vision, iot);

    //Fase 3: i dati vengono distribuiti tramite piattaforma PubSube e Notarizzati su Blockchain
    bcNotarization.notarize(digest);
    pubSub.publish(digest);
}

main();
