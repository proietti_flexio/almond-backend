//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
var rest = require('./rest.js');
var api = require('./api.json');

// questo metodo dovrebbe espandersi ad una ricerca (motivata da analisi dei trend, di mercato, psicologiche, sociologiche...)
// di un criterio di quantificazione dell' opinione degli abitanti della città (quartiere, area pubblica..)
function extract(text) {

    var payload = {
        "document": {
            "type": "PLAIN_TEXT",
            "content": text
        }
    }

    return rest.callToServiceAt(api.gcloudnaturallanguage.analyzeSentiment, undefined, "post", JSON.stringify(payload), 'application/json' );
}

module.exports = {
    extract: extract
}