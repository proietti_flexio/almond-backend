//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
var rest = require('./rest.js');
var api = require('./api.json')

function publish(data) {
    rest.callToServiceAt(api.gcloudpubsub.topics.publish + process.env.PUBSUB_PROJECT + api.gcloudpubsub.topics.topics + process.env.PUBSUB_TOPIC + ":publish", undefined
        , "post", JSON.stringify({
            messages: [
                {
                    data: new Buffer.from(JSON.stringify(data)).toString('base64')
                }
            ]
        }), "application/json", true)
}

module.exports = {
    publish: publish
}