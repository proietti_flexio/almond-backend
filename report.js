//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
reportStrings = require("./reportStrings.json");

function create(air, soc, vision, iot) {
    // questi valori pesano il contributo in ogni ambito
    // si potrebbe implementare un sistema dinamico e automatico di ricalcolo dei pesi    
    var weights = [.3, .1, .3, .3];

    A = evaluate(air, 0)
    S = evaluate(soc, 1)
    V = evaluate(vision, 2)
    I = evaluate(iot, 3)

    var grade = A * weights[0] + S * weights[1] + V * weights[2] + I * weights[3];

    //viene restituito il report compilato
    if (grade > 60) {
        return {
            wh: weights,
            summary: reportStrings.ORCHARD,
            report: {
                $air: {
                    report: air,
                    grade: A
                },
                $soc: {
                    report: soc,
                    grade: S
                },
                $vision: {
                    report: vision,
                    grade: V
                },
                $iot: {
                    report: iot,
                    grade: I
                }
            }
        }
    } else {
        return {
            summary: reportStrings.GLADE,
            report: {
                $air: {
                    report: air,
                    grade: A
                },
                $soc: {
                    report: soc,
                    grade: S
                },
                $vision: {
                    report: vision,
                    grade: V
                },
                $iot: {
                    report: iot,
                    grade: I
                }
            }
        }
    }
}

// In base al dato da API e all' ambito si procede con la scelta di un voto complessivo per l' area
function evaluate(data, category) {
    return parseInt(20 + Math.random()*80);
}

module.exports = {
    create: create
}