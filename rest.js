//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
var environment = process.env;
var axios = require('axios');

//modulo di servizio per chiamate REST

function configFactory(url, params, method, data, type) {

    var headers = {
        'apikey': environment.API_KEY
    };


    if (type) {
        headers['Content-Type'] = type;
    }

    return {
        method: method || 'get',
        url: environment.API_BASE_URL + url,
        headers: headers,
        params: params,
        data: data
    }
};

function callToServiceAt(url, params, method, data, type, print) {
    return new Promise((ok, ko) => {
        axios(configFactory(url, params, method, data, type))
            .then(function (response) {
                print = true;
                if (!print) {
                    console.log(JSON.stringify(response.data));
                    console.log("---------------------------");
                }
                ok(response.data)

            })
            .catch(function (error) {
                console.log(error);
                ko(error)
            })

    })

}

module.exports = {
    configFactory: configFactory,
    callToServiceAt: callToServiceAt
}