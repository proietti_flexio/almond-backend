//### AUTHORS
//Alessio Proietti <alessio.proietti@protonmail.com>
var nat = require('./naturalLanguage.js');

// questa routine potrebbe implementare una chiamata ad un servizio (o implementare il servizio stesso)
// di crawling nelle interazione social degli account della PA cliente
function crawlSocialMedia() {

    return [
        "Da piazza Barberini a Trinità dei Monti, potenzialmente una delle passeggiate più belle del mondo. Immaginate una coppia di fidanzati, turisti: la devono percorrere in fila indiana, tra le macchine e lo smog. Ti credo che poi scelgono, che so, Parigi. Proprio non ci arrivano...",
        "Non so se il mio esempio possa significare qualcosa ma...zona Centro bar/tavole calde/pub già hanno allargato lo spazio dei tavolini all'aperto, occupato le strisce blu e i posti per gli scooter.",
        "Roma, è una delle mie città preferite al mondo",
        "Ho lasciato a Roma il mio cuore anni fa...",
        "segnaliamo, denunciamo l'inciviltá quotidiana, sosteniamo iniziative per riaffermare la legalitá a Roma. Per il rispetto della Legge che é rispetto degli altri.",
        "Favoloso"
    ]
}

// i dati social grezzi vengono immessi nel motore di analisi del linguaggio naturale per estrarre una analisi della reputazione di cui
// la città (quartiere, area...) gode presso chi la fruisce
function harvest() {
    return nat.extract(crawlSocialMedia()[parseInt(Math.random()* crawlSocialMedia().length)]);
}


module.exports = {
    harvest: harvest
}